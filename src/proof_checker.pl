% ⇊ [[file:../proof_checker.org::722][Scope construction:1]] ⇊
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Types:

   line : LineNumber -> Depth -> Formula -> Justification -> Line
   type Proof = [Line]

   type Stack a = [a]
   a - [Tree a] : Tree a
   type Scope = Stack (Tree [Line])
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


% curScopeDepth : Scope -> Int -> Prop
curScopeDepth([], 0).
curScopeDepth([ Lines-_ | _], D) :- Lines = [ line(_,D,_,_) | _].

%%%%%%%%%%%%
% addToScope : Line -> Scope -> Scope -> Prop
%%%%%%%%%%%%

% empty scope base cases
addToScope(L, [], [ [L]-[], []-[] ]) :-
        % empty scope but start with subproof
        L = line(1,2,_,_),
        !.

addToScope(L, [], [ [L]-[] ]) :- !.

% Same depth
addToScope(L, Scope, NScope) :-
        % line not a premise and keeps depth
        L = line(_, D, _, J),
        J \= premise,
        curScopeDepth(Scope, D),
        !,
        Scope  = [      Lines -Subproofs | Ts],
        NScope = [ [L | Lines]-Subproofs | Ts]. % Placed at head

addToScope(L, Scope, NScope) :-
        % an extra premise without deepening can only occur at top level scope
        % natural deduction rules with subproof only discharge one premise
        % at top level they will never be discharged
        % can be introduced anywhere although tradicionally on first lines
        Scope = [ Lines-Subproofs ], % only 1 scope ⇒ top level
        L = line(_, D, _, premise),
        curScopeDepth(Scope, D),
        !,
        NScope = [ [L | Lines]-Subproofs ]. % Placed at head

addToScope(L, Scope, NScope) :-
        % a premise without deepening and NOT on top scope
        % must be interpreted to mean current scope is closed
        % and a fresh one is opened
        L = line(_, D, _, premise),
        curScopeDepth(Scope, D),
        !,
        Scope  = [ Lines-_Subproofs, Lines2-Subproofs2       | Ts],
        % archived current scope in parent's children
        % create a fresh scope with same parent
        NScope = [ [L]-[], Lines2-[Lines-[] | Subproofs2 ]  | Ts].

% Deeper
addToScope(L, Scope, NScope) :-
        % line is child (deeper) fresh scope
        L = line(_, D, _, _),
        curScopeDepth(Scope, D2),
        D is D2 + 1,
        !,
        NScope = [ [L]-[] | Scope].

% Shallower
addToScope(L, Scope, NScope) :-
        % line leaves scope by decreasing depth
        L = line(_, D, _, _),
        curScopeDepth(Scope, D2),
        D is D2 - 1,
        !,
        Scope  = [ Lines-_Subproofs, Lines2-Subproofs2     | Ts],
        % archived current scope in parent's children
        % added L to parent scope
        % note how we forget subproofs of the scope we left, since a parent can only
        % reference the full child's subproof.
        NScope = [ [L | Lines2]-[Lines-[] | Subproofs2 ]  | Ts].
% ⇈ [[file:../proof_checker.org::722][Scope construction:1]] ⇈

% ⇊ [[file:../proof_checker.org::809][Scope access:1]] ⇊
% getFormula : Scope -> Nat -> Formula -> Prop
getFormula([], _, _) :- false.              %todo fails should give some info
getFormula([ Lines-_Subproofs | _Ts], N, F) :- member(line(N, _, F, _), Lines), !.
getFormula([ _                |  Ts], N, F) :- getFormula(Ts, N, F).

%% ------------------------------------------------------------------------
%% Getting a subproof makes sure that F1 is a premise

% getSubproof : Scope -> Nat-Nat -> Formula-Formula -> Prop
getSubproof([], _, _) :- false.
getSubproof([ _Lines-Subproofs | _Ts], N1-N2, F1-F2) :- searchSubproof(Subproofs, N1-N2, F1-F2), !.
getSubproof([ _                |  Ts], N1-N2, F1-F2) :- getSubproof(Ts, N1-N2, F1-F2).

% searchSubproof : [Tree [Line]] -> Nat-Nat -> Formula-Formula -> Prop
searchSubproof([], _, _) :- false.
searchSubproof([ Lines-_ | _Ts], N1-N2, F1-F2) :- searchBothLines(Lines, N1-N2, F1-F2), !.
searchSubproof([_Lines-_ |  Ts], N1-N2, F1-F2) :- searchSubproof(Ts, N1-N2, F1-F2).

% searchBothLines : [Line] -> Nat-Nat -> Formula-Formula -> Prop
searchBothLines([], _, _) :- false.
searchBothLines([ L | Ls], N1-N2, F1-F2) :-
        L = line(N1,_,F1,premise), !,
        ( ground(F2)
        ->
          true
        ; searchBothLines(Ls, N1-N2, F1-F2)).

searchBothLines([ L |  Ls], N1-N2, F1-F2) :-
        L = line(N2,_,F2,_), !,
        ( ground(F1)
        ->
          true
        ; searchBothLines(Ls, N1-N2, F1-F2)).

searchBothLines([_L | Ls], N1-N2, F1-F2) :- searchBothLines(Ls, N1-N2, F1-F2).
% ⇈ [[file:../proof_checker.org::809][Scope access:1]] ⇈

% ⇊ [[file:../proof_checker.org::848][Validate a line:1]] ⇊
%% ------------------------------------------------------------------------
%% Process proof

% validLine : Line -> Scope -> Scope -> Prop
validLine(L, Scope, NScope) :-
        addToScope(L, Scope, NScope),
        L = line(_, _, _, J),
        J =.. [Rule | _],
        validate(Rule, L, NScope).

% validate : Rule -> Line -> Scope -> Prop.
validate(premise, _L, _Scope) :- !.    % nothing to verify on a premise

validate(Rule, L, _Scope) :-
        % Excluded middle
        Rule = '_ ∨ ¬_',
        !,
        L = line(_, _, F0, _),
        call(Rule,F0),
        !.                      % has alternatives

validate(Rule, L, Scope) :-
        '1-line-rule'(Rule),
        !,
        L = line(N0, _, F0, J),
        J =.. [Rule, N1],       % '∧E'(2)
        % since L is already in the scope, we make sure it's not self-referential
        N1 \= N0,
        getFormula(Scope, N1, F1),
        call(Rule, F1, F0),
        !.                      % '∧E', '∨I' have alternatives.

validate(Rule, L, Scope) :-
        '2-line-rule'(Rule),
        !,
        L = line(N0, _, F0, J),
        J =.. [Rule, N1, N2],       % '→'(1,3)
        % since L is already in the scope, we make sure it's not self-referential
        N1 \= N0, N2 \= N0,
        getFormula(Scope, N1, F1),
        getFormula(Scope, N2, F2),
        call(Rule, F1, F2, F0).

validate(Rule, L, Scope) :-
        '1-interval-rule'(Rule),
        !,
        L = line(N0, _, F0, J),
        J =.. [Rule, N1-N2],       % '→I'(2-6)
        % since L is already in the scope, we make sure it's not self-referential
        N1 \= N0, N2 \= N0,
        getSubproof(Scope, N1-N2, F1-F2),
        call(Rule, subproof(F1,F2), F0).

validate(Rule, L, Scope) :-
        '1-line-2-interval-rule'(Rule),
        !,
        L = line(N0, _, F0, J),
        J =.. [Rule, N1, N2-N3, N4-N5],       % '∨E'(3, 4-6, 7-9)
        % since L is already in the scope, we make sure it's not self-referential
        \+ member(N0, [N1, N2, N3, N4, N5]),
        getFormula(Scope, N1, F1),
        getSubproof(Scope, N2-N3, F2-F3),
        getSubproof(Scope, N4-N5, F4-F5),
        call(Rule, F1, subproof(F2,F3), subproof(F4,F5), F0).

'1-line-rule'('∧E').
'1-line-rule'('∨I').
'1-line-rule'('¬E').
'1-line-rule'('⊥E').
'1-line-rule'('R').

'2-line-rule'('∧I').
'2-line-rule'('⊥I').
'2-line-rule'('→E').

'1-interval-rule'('¬I').
'1-interval-rule'('→I').

'1-line-2-interval-rule'('∨E').
% ⇈ [[file:../proof_checker.org::848][Validate a line:1]] ⇈

% ⇊ [[file:../proof_checker.org::932][Validate a full proof:1]] ⇊
%%%%%%%%%%%%
% Clearest declarative definition
% only doesn't check last line has identation 1
% validProof : Proof -> Prop
validProof(Proof) :- foldl(validLine, Proof, [], _FinalScope).


%%%%%%%%%%%%
% Validate with Error Info
% validProof_info : Proof -> Maybe Line -> Prop
validProof_info(Proof, InvalidLine) :-
        validProof_worker(Proof, [], InvalidLine).

% validProof_worker : Proof -> Scope -> Maybe Line -> Prop
validProof_worker([], _, nothing).

validProof_worker([L | Ls], Scope, InvalidLine) :-
        validLine(L, Scope, NScope),
        (   Ls = []
        ->  L = line(_,1,_,_)
        ;   true
        ),
        !,
        validProof_worker(Ls, NScope, InvalidLine).

validProof_worker([L | _Ls], _Scope, L).
% ⇈ [[file:../proof_checker.org::932][Validate a full proof:1]] ⇈
