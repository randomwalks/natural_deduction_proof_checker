% ⇊ [[file:../proof_checker.org::507][Grammar:1]] ⇊
:- use_module(library(dcgs)).
:- use_module(library(pio)).     % phrase_from_file
:- use_module(library(charsio)). % char_type
:- use_module(library(lists)).   % member

% :- use_module(library(debug)).

% accepts empty proof
proof([L | Ls]) --> line(L), ("\n" | ""), !, proof(Ls).
proof([])       --> ws.

%% line : LineNumber -> Scope -> Formula -> Justification -> Line
line(line(N, S, F, J)) -->
        number(N),
        scope(S),
        formula(F),
        justification(J),                  % lack of justification ⇒ premise
        (   comment, horizontal_separator  % annotate premise lines
          | comment                        % annotate invalid proofs
          | horizontal_separator           % horizontal separator is ignored, just visual queue
          | ""
        ), !.

ws --> " ", !, ws | "".            % whitespace consumer

%% ------------------------------------------------------------------------

%% Number
%% number : [Char] -> Prop, "" not accepted
number(N) --> consumer(Cs, decimal_digit), { number_chars(N, Cs) }.
%% ------------------------------------------------------------------------
%% Scope

scope(S) --> vbar, scope(T), !, { S is T+1 }.
scope(S) --> vbar, !, { S is 1 }.

vbar --> ("│" | "|" | "╷"), ws.

%% ------------------------------------------------------------------------
%% Formulas

formula('→'(F1,F2)) --> f1(F1), implies, !, formula(F2). % → right associative
formula(F)           --> f1(F).

% ∨ left associative, left recursion eliminated, term constructed using accumulator
f1(F)       --> f2(F1), f1rec(F1, F).
f1rec(F1,F) --> or, !, f2(F2), f1rec('∨'(F1,F2), F).
f1rec(F1,F) --> { F1 = F }.

% ∧ left associative, left recursion eliminated, term constructed using accumulator
f2(F)        --> f3(F1), f2rec(F1, F).
f2rec(F1, F) --> and, !, f3(F2), f2rec('∧'(F1,F2), F).
f2rec(F1, F) --> { F1 = F }.

f3('¬'(F))     --> not, !, f3(F).
f3(F)          --> (atom(F) | parenthesis(F)), !.

parenthesis(F) --> '(', formula(F), ')'. % note these are predicates not dcg consumers.

atom(F) --> top(F),  !        % since "T" will also match a constant
        | bottom(F), !        % since "F" will also match a constant
        | constant(F).

top('⊤')    --> ("⊤" | "T"), ws.
bottom('⊥') --> ("⊥" | "F"), ws.

%% constant :: Atom -> Prop, "" not accepted
constant(F) --> consumer(Cs, alphabetic), { atom_chars(F, Cs) }.

%% Low to high precedence
implies --> ("→" | "->"), ws.
or      --> ("∨"  | "|"),  ws.
and     --> ("∧"  | "&"),  ws.
not     --> ("¬"  | "~"),  ws.

'(' --> "(", ws.
')' --> ")", ws.

%% ------------------------------------------------------------------------
%% Justifications

justification(J) -->
        proves,
        ( premise(J)
        | 'excluded middle'(J)
        | '1-line-justification'(J)
        | '2-line-justification'(J)
        | '1-interval-justification'(J)
        | '1-line-2-interval-justification'(J)
        ), !.
justification(premise) --> "".  % no justification means it's implicit premise

proves --> ("⊣" | "#"), ws.

premise(premise) --> ("Premise" | "premise" ), ws.
'excluded middle'(J)      --> "_ ∨ ¬_", { J =.. ['_ ∨ ¬_'] }.
'1-line-justification'(J) --> '1-line-rule'(Rule), !, number(N), { J =.. [Rule, N] }.
'2-line-justification'(J) --> '2-line-rule'(Rule), !,
        number(N1), '，| ws', number(N2),
        { J =.. [Rule, N1, N2] }.

'1-interval-justification'(J) --> '1-interval-rule'(Rule), !,
         '[', number(N1), '–', number(N2), ']',
         { J =.. [Rule, N1-N2] }.

'1-line-2-interval-justification'(J) --> '1-line-2-interval-rule'(Rule), !,
        number(N1),
        '，| ws',
        '[', number(N2), '–', number(N3), ']',
        '，| ws',
        '[', number(N4), '–', number(N5), ']',
        { J =.. [Rule, N1, N2-N3, N4-N5] }.

% Same predicate name as in check proof but with different arity
'1-line-rule'('∧E') --> and, "E",           '，| ws'.
'1-line-rule'('∨I') --> or, "I",            '，| ws'.
'1-line-rule'('¬E') --> not, "E",           '，| ws'.
'1-line-rule'('⊥E') --> bottom(_), "E",     '，| ws'.
'1-line-rule'('R')  --> "R",                '，| ws'.

'2-line-rule'('∧I')  --> and, "I",          '，| ws'.
'2-line-rule'('⊥I')  --> bottom(_), "I",    '，| ws'.
'2-line-rule'('→E') --> implies, "E",      '，| ws'.

'1-interval-rule'('¬I')  --> not, "I",      '，| ws'.
'1-interval-rule'('→I') --> implies, "I",  '，| ws'.

'1-line-2-interval-rule'('∨E') --> or, "E", '，| ws'.


'，| ws' --> ("," | "" ), ws.   % abusing UNICODE
'[' --> "[", ws.
']' --> "]", ws.
'–' --> "-", ws.                % abusing UNICODE

%% ------------------------------------------------------------------------
% Utilities
% A greedy (biggest match) consumer of a specific family of characters, consumes at least 1.
consumer([C | Cs], Type)    --> [C], { char_type(C, Type) }, consumerRec(Cs, Type).

consumerRec([C | Cs], Type) --> [C], { char_type(C, Type) }, !, consumerRec(Cs, Type).
consumerRec([],      _Type) --> ws.

horizontal_separator -->
        "\n",
        ws,
        vbar_consumer,
        ("├" | ""),
        dash, dash_consumer, % #dash >= 1
        !.

%% #vbar >= 0
vbar_consumer --> vbar, !, vbar_consumer
                | "".

%% #dash >= 0
dash_consumer --> dash, !, dash_consumer
                | "".

dash --> ("─" | "-"), ws.

%% to annotate knowingly invalid proofs
comment --> "<<<", non_newline.
non_newline --> [C], { C \= '\n' }, !, non_newline.
non_newline --> "".
% ⇈ [[file:../proof_checker.org::507][Grammar:1]] ⇈
