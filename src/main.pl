% ⇊ [[file:../proof_checker.org::398][Main:1]] ⇊
:- use_module(library(files)).
:- use_module(library(format)).
:- use_module(library(lists)).

:- use_module(grammar).
:- use_module(natural_rules).
:- use_module(proof_checker).
:- use_module(utilities).

main         :- check_all_valid,   halt.
main_invalid :- check_all_invalid, halt.

check_all_valid :-
      Dir = "../examples/valid/",
      directory_files(Dir, Files),
      maplist(check_proof_file(Dir), Files),
      format("~s~n", ["All proofs valid."]).

check_all_invalid :-
      Dir = "../examples/invalid/",
      directory_files(Dir, Files),
      sort(Files, FilesSorted), % apparently they don't show up in lexicographic order
      maplist(check_proof_file(Dir), FilesSorted).

check_proof_file(Dir, Filename) :-
        append(Dir, Filename, Path),
        % since we want the left overs that might have failed parsing
        phrase_from_file(read_file(S), Path), % get full string from file
        phrase(proof(Proof), S, Rest),
        (   Rest \= []
        ->  format("~s~n~s~n~s~n", [Filename, "Parse error in:", Rest])
        ;   (   validProof_info(Proof, InvalidLine),
               (  InvalidLine \= nothing
               -> format("~s~n~s~n~w~n~n", [Filename, "Validation error in:", InvalidLine])
               ;  true  % happy path
                )
             )
        ).

%% DCG to read a file into a full string.
read_file([ C | Cs]) --> [C], !, read_file(Cs).
read_file([]) --> "".

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   ?- main.
   %@ All proofs valid.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
% ⇈ [[file:../proof_checker.org::398][Main:1]] ⇈
