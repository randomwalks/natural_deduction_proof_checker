% ⇊ [[file:../proof_checker.org::966][Utilities:1]] ⇊
%% ------------------------------------------------------------------------
% Other Utilities
% take :: Nat -> [a] -> [a] -> Prop
take(0,  _, []) :- !.
take(_, [], []) :- !.
take(N, [A | As], [A | Ls]) :-
        N2 is N-1,
        take(N2, As, Ls).

drop(0, Ls, Ls) :- !.
drop(_, [], []) :- !.
drop(N, [_A | As], Ls) :-
        N2 is N-1,
        drop(N2, As, Ls).

non_alphabetic_test(Char) :- \+ char_type(Char, alphabetic).
% ⇈ [[file:../proof_checker.org::966][Utilities:1]] ⇈
