% ⇊ [[file:../proof_checker.org::680][Natural deduction rules:1]] ⇊
%% ------------------------------------------------------------------------
%% Natural Deduction rules
'∧I'(X,Y, '∧'(X,Y)).

'∧E'('∧'(X,_Y), X).
'∧E'('∧'(_X,Y), Y).

'∨I'(X, '∨'(X,_)).
'∨I'(Y, '∨'(_,Y)).

'∨E'('∨'(X,Y), subproof(X,Z), subproof(Y,Z), Z).

'¬I'(subproof(X,'⊥'), '¬'(X)).
'¬E'('¬'('¬'(X)), X).

% i believe indexing makes a match on first clause not spawn an alternative choice point
% on excluded middle it doesn't work because it's only top level functor?
'⊥I'('¬'(X), X, '⊥').
'⊥I'(X, '¬'(X), '⊥').

'⊥E'('⊥', _X).

'→I'(subproof(X,Y), '→'(X,Y)).
'→E'('→'(X,Y), X, Y).

'R'(X,X).

%%%%%%%%%%%% To shorten long proofs: Excluded middle
'_ ∨ ¬_'('∨'('¬'(X), X)).
'_ ∨ ¬_'('∨'(X, '¬'(X))).
% ⇈ [[file:../proof_checker.org::680][Natural deduction rules:1]] ⇈
